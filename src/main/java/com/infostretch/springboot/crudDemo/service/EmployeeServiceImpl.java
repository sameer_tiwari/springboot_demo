package com.infostretch.springboot.crudDemo.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.infostretch.springboot.crudDemo.dao.EmployeeDAO;
import com.infostretch.springboot.crudDemo.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeDAO employeeDAO;

	public EmployeeServiceImpl(EmployeeDAO dao) {
		employeeDAO = dao;
	}

	@Override
	@Transactional
	public List<Employee> finadAll() {
		// TODO Auto-generated method stub
		return employeeDAO.finadAll();
	}

	@Override
	@Transactional
	public Employee findById(int id) {
		// TODO Auto-generated method stub
		return employeeDAO.findById(id);
	}

	@Override
	@Transactional
	public void save(Employee employee) {
		// TODO Auto-generated method stub
		employeeDAO.save(employee);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub

		employeeDAO.delete(id);
	}

}
