package com.infostretch.springboot.crudDemo.service;

import java.util.List;

import com.infostretch.springboot.crudDemo.entity.Employee;

public interface EmployeeService {

	public List<Employee> finadAll();

	public Employee findById(int id);

	public void save(Employee employee);

	public void delete(int id);
}
