package com.infostretch.springboot.crudDemo.rest;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.infostretch.springboot.crudDemo.entity.Employee;
import com.infostretch.springboot.crudDemo.service.EmployeeService;

@RestController
@RequestMapping("/api")
public class EmployeeRestController {

  private EmployeeService service;

  public EmployeeRestController(EmployeeService employeeService) {
    service = employeeService;
  }

  @GetMapping("/employees")
  public List<Employee> getEmployees() {
    return service.finadAll();
  }

  @GetMapping("/employees/{id}")
  public Employee getEmployee(@PathVariable int id) {

    Employee employee = service.findById(id);

    if (employee == null) {
      throw new RuntimeException("Employee not found");
    }
    return employee;
  }

  @PostMapping("/employees")
  public Employee save(@RequestBody Employee employee) {
    employee.setId(0);
    service.save(employee);
    return employee;
  }

  @PutMapping("/employees")
  public Employee update(@RequestBody Employee employee) {

    service.save(employee);
    return employee;
  }

  @DeleteMapping("/employees/{id}")
  public String delete(@PathVariable int id) {
    service.delete(id);
    return "Delete employee : id -" + id;
  }

}
