package com.infostretch.springboot.crudDemo.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.infostretch.springboot.crudDemo.entity.Employee;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

  private EntityManager manager;

  @Autowired
  public EmployeeDAOImpl(EntityManager entityManager) {

    manager = entityManager;
  }

  @Override
  @Transactional
  public List<Employee> finadAll() {

    Session current = manager.unwrap(Session.class);

    Query<Employee> query = current.createQuery("from Employee", Employee.class);

    List<Employee> list = query.getResultList();

    return list;
  }

  @Override
  public Employee findById(int id) {

    Session currentSession = manager.unwrap(Session.class);

    return currentSession.get(Employee.class, id);
  }

  @Override
  public void save(Employee employee) {

    Session currentSession = manager.unwrap(Session.class);

    currentSession.saveOrUpdate(employee);

  }

  @Override
  public void delete(int id) {
    Session currentSession = manager.unwrap(Session.class);
    Query query = currentSession.createQuery("delete from Employee where id=:employeeId");
    query.setParameter("employeeId", id);
    query.executeUpdate();
  }

}
